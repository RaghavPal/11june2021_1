<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TestSuiteOne</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b65f8346-2fb0-4aeb-9bf1-7f1d60402fd7</testSuiteGuid>
   <testCaseLink>
      <guid>d5cb59af-44d7-4c37-a8c3-153c26bb9813</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/One/TestOne</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23def9e5-bf61-42f0-9c1d-b53294adf664</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/One/TestTwo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
